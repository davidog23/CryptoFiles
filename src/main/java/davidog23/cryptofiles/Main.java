package davidog23.cryptofiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.DosFileAttributes;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.crypto.*;

/**
 * Clase de entrada al programa.
 * Guia de uso: ruta modo <-c "name" "password">
 * En caso de no especificar se usa un nombre y una contraseña por defecto.
 */
public class Main {
    public static void main(String[] args) {
        final String guia = "Guia de uso: ruta modo <-c \"name\" \"password\">";

        try {
            /** PROCESANDO ARGUMENTOS */
            if (args.length != 2 && args.length != 5) throw new IllegalArgumentException("Número de parametros inválido. " + guia);
            File dir = null;
            int mode = -1;
            String pass = "password";
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("cifrar") && mode == -1) {
                    mode = Cipher.ENCRYPT_MODE;
                } else if (args[i].equals("descifrar") && mode == -1) {
                    mode = Cipher.DECRYPT_MODE;
                } else if ((new File(args[i])).exists() && dir == null) {
                    dir = new File(args[i]);
                } else if (args[i].equals("-c")){
                    pass = args[i + 1];
                    i++;
                } else {
                    throw new IllegalArgumentException("Argumento no válido. " + guia);
                }
            }
            if (dir == null) { throw new IllegalArgumentException("Ruta introducida no válida. Por favor, introduzca la ruta completa entre comillas dobles."); }
            if (mode == -1) { throw new IllegalArgumentException("ERROR. No se ha especificado el modo."); }
            File[] names = dir.listFiles(pathname -> pathname.getName().endsWith(".name"));
            Arrays.parallelSort(names, (f1, f2) -> f1.getName().compareToIgnoreCase(f2.getName()));
            String name = names.length > 0 ? names[0].getName().substring(0, names[0].getName().lastIndexOf('.')) : null;
            if (name == null) { throw new FileNotFoundException("No se ha encontrado el archivo que corresponde al nombre de la clave"); }

            /** RECUPERACION O CREACIÓN DE LA CLAVE */
            long start = System.currentTimeMillis();
            final String separator = System.getProperties().getProperty("file.separator");
            final String filenameKeyStoreDir = System.getProperties().getProperty("user.home") + separator + ".cryptofiles";
            final String filenameKeyStore =  filenameKeyStoreDir + separator + "key.keystore";

            //Crea la carpeta donde guarda el archivador.
            File keyStoreDir = new File(filenameKeyStoreDir);
            if (!keyStoreDir.exists()) {
                keyStoreDir.mkdir();
            }

            //Crea el archivador.
            final KeyStore keyStore = Util.getArchivador(filenameKeyStore, mode);

            //Visibiliza el archivador y la carpeta.
            File keyStoreFile = new File(filenameKeyStore);
            if (Files.readAttributes(keyStoreFile.toPath(), DosFileAttributes.class).isHidden()) {
                Files.setAttribute(keyStoreFile.toPath(), "dos:hidden", false);
                Files.setAttribute(keyStoreDir.toPath(), "dos:hidden", false);
            }

            final SecretKey key = Util.getSecretKey(filenameKeyStore, keyStore, name, pass);

            //Oculta el archivador y la carpeta.
            if (!Files.readAttributes(keyStoreFile.toPath(), DosFileAttributes.class).isHidden()) {
                Files.setAttribute(keyStoreFile.toPath(), "dos:hidden", true);
                Files.setAttribute(keyStoreDir.toPath(), "dos:hidden", true);
            }

            /** COMIENZO DEL CIFRADO */
            if (mode == Cipher.DECRYPT_MODE) { Files.setAttribute(dir.toPath(), "dos:hidden", false); }
            boolean terminated;
            if (mode == Cipher.DECRYPT_MODE) {
                Util.recursiveDescifrado(dir, key);
                Util.POOL.shutdown();
                terminated = Util.POOL.awaitTermination(600, TimeUnit.SECONDS);
            } else {
                Util.recursiveCifrado(dir, key);
                Util.POOL.shutdown();
                terminated = Util.POOL.awaitTermination(600, TimeUnit.SECONDS);
            }
            Scanner s = new Scanner(System.in);
            while (!terminated) {
                try {
                    System.out.println("Después de 10 minutos, aún no se ha terminado.\nInserte nuevo tiempo de espera en segundos enteros positivos: ");
                    int secs = s.nextInt();
                    if (secs < 0) throw new InputMismatchException();
                    terminated = Util.POOL.awaitTermination(secs, TimeUnit.SECONDS);
                    if (secs == 0) terminated = true;
                } catch (InputMismatchException e) {
                    System.out.println("Valor introducido no válido.");
                }
            }
            if (mode == Cipher.ENCRYPT_MODE) {
                Files.setAttribute(dir.toPath(), "dos:hidden", true);
                new FileOutputStream(dir.toPath().resolve(name + ".name").toFile()).close();
            }
            System.out.println("El programa ha tardado " + (System.currentTimeMillis() - start)/1000 + " segundos");
            /** FIN DEL CIFRADO */
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | InterruptedException e) {
            if (e instanceof FileNotFoundException) System.err.println(e.getMessage());
            else e.printStackTrace();
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        } catch (UnrecoverableEntryException e) {
            if (e instanceof UnrecoverableKeyException) System.err.println("Contraseña erronea");
            else System.err.println("Usuario incorrecto");
        }
    }
}
