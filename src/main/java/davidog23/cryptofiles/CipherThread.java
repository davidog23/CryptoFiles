package davidog23.cryptofiles;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Representa un hilo de cifrado/descifrado.
 * Created by David on 07/06/2016.
 */
public class CipherThread implements Runnable {

    private File file;
    private SecretKey key;
    private int mode;

    public CipherThread(File f, SecretKey key, int mode) {
        file = f;
        this.mode = mode;
        this.key = key;
    }

    @Override
    public void run() {
        try {
            if (mode == Cipher.ENCRYPT_MODE) {
                String name = file.getAbsolutePath();
                Util.cipher(new FileInputStream(file), new FileOutputStream(name + ".enc"), key, mode);
                file.delete();
                Files.setAttribute(Paths.get(name + ".enc"), "dos:hidden", true);
            } else if (mode == Cipher.DECRYPT_MODE){
                Files.setAttribute(file.toPath(), "dos:hidden", false);
                String name = file.getAbsolutePath();
                Util.cipher(new FileInputStream(file), new FileOutputStream(name.substring(0, name.lastIndexOf('.'))), key, mode);
                file.delete();
            }
        } catch (IOException e) {
            System.err.println("Error en la ejecución de un thread de cifrado/descifrado en el archivo: " + file.getName());
            e.printStackTrace();
        }
    }
}
