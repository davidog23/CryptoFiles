package davidog23.cryptofiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.SecretKeyEntry;
import java.security.cert.CertificateException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.*;

public class Util {
    /**
     * Pool de threads para los hilos de cifrado/descifrado
     */
    public static final ExecutorService POOL = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() > 1 ? Runtime.getRuntime().availableProcessors() - 1 : 1);

    /**
     * Método que cifra un archivo y guarda el resultado en otro.
     * @param fIn El FileInputStream que contine los datos del archivo a cifrar
     * @param fOut El FileOutputStream creado con la ruta deseada.
     * @param key La llave simetrica de cifrado AES.
     * @throws IOException
     */
    public static void cipher(FileInputStream fIn, FileOutputStream fOut, SecretKey key, int mode) throws IOException {
        boolean hasFatalException = false;
        CipherOutputStream cOut = null;
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(mode, key);
            cOut = new CipherOutputStream(fOut, cipher);
            int i;
            byte[] buffer = new byte[8096];
            while ((i = fIn.read(buffer)) > 0) {
                cOut.write(buffer, 0 , i);
            }
        } catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException e) {
            System.err.println("Error initializing the cipher.");
            e.printStackTrace();
            hasFatalException = true;
        } finally {
            try {
                if (cOut != null) cOut.close();
                fIn.close();
            } catch (IOException e) {
                System.err.println("Unexpected error closing streams at cifrar method");
                e.printStackTrace();
                hasFatalException = true;
            } finally {
                if (hasFatalException) System.exit(-1);
            }
        }
    }

    /**
     * Analiza si la entrada es un archivo o una carpeta y en el primer caso lo cifra y en el segundo cifra entero el arbol de archivos y subcarpetas inferiores.
     * @param dir Directorio o fichero a cifrar.
     * @param key La llave "AES" usada para cifrar.
     * @see #cipher(FileInputStream, FileOutputStream, SecretKey, int)
     * @see #recursiveDescifrado(File, SecretKey)
     * @see CipherThread
     */
    public static void recursiveCifrado(File dir, SecretKey key) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        try {
            if(dir.isDirectory()) {
                File[] dirList = dir.listFiles();
                if (dirList == null) throw new SecurityException("No se ha podido acceder a la carpeta: " + dir.getAbsolutePath());
                for(File tmp : dirList) {
                    recursiveCifrado(tmp, key);
                }
            } else if (!dir.getName().endsWith(".enc")) {
                POOL.execute(new CipherThread(dir, key, Cipher.ENCRYPT_MODE));
            }
        } catch (SecurityException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Analiza si la entrada es un archivo o una carpeta y en el primer caso lo descifra y en el segundo descifra entero el arbol de archivos y subcarpetas inferiores.
     * @param dir Directorio o fichero a descifrar.
     * @param key La llave "AES" necesaria para descifrar.
     * @see #recursiveCifrado(File, SecretKey)
     * @see #cipher(FileInputStream, FileOutputStream, SecretKey, int)
     * @see CipherThread
     */
    public static void recursiveDescifrado(File dir, SecretKey key) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        try {
            if(dir.isDirectory()) {
                File[] dirList = dir.listFiles();
                if (dirList == null) throw new SecurityException("No se tiene acceso a la carpeta: " + dir.getAbsolutePath());
                for(File tmp : dirList) {
                    recursiveDescifrado(tmp, key);
                }
            } else if (dir.getName().endsWith(".enc")) {
                POOL.execute(new CipherThread(dir, key, Cipher.DECRYPT_MODE));
            }
        } catch (SecurityException e) {
            System.err.println(e.getMessage());
        }
    }
    
    /**
     * Genera una llave secreta simétrica.
     * @param algorithm Algoritmo de la clave generada.
     * @param size Tamaño en bits de la llave generada.
     * @return La llave secreta (simétrica) generada
     */
    public static SecretKey generateKey(String algorithm, int size) throws NoSuchAlgorithmException
    {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
        keyGenerator.init(size);
        return keyGenerator.generateKey();
    }
    
    /**
     * Busca si existe una llave creada con anterioridad y, si es así, la carga. Si no, la crea y la almacena en un archivador de llaves.
     * @param filePath Ruta y nombre del archivo del archivador de llaves.
     * @param ks Instancia del archivador de llaves.
     * @param pw Contraseña del archivador y de la llave.
     * @return La llave secreta.
     * @see #getKey(KeyStore, String, String)
     * @see #generateKey(String, int)
     * @see #saveSecretKey(String, KeyStore, SecretKey, String, String)
     */
    public static SecretKey getSecretKey(String filePath, KeyStore ks, String name, String pw) throws NoSuchAlgorithmException, UnrecoverableEntryException, KeyStoreException, CertificateException, IOException {
        if(ks.size() > 0 && ks.containsAlias(name)){
            return getKey(ks, name, pw);
        } else {
            SecretKey key = generateKey("AES", 128);
            saveSecretKey(filePath, ks, key, name, pw);
            return key;
        }
    }

    /**
     * Devuelve la llave secreta del archivador.
     * @param ks Instancia del archivador de llaves.
     * @param pw Contraseña de la llave.
     * @return La llave secreta del archivador.
     */
    public static SecretKey getKey(KeyStore ks, String name, String pw) throws NoSuchAlgorithmException, UnrecoverableEntryException, KeyStoreException {
        KeyStore.Entry entry = ks.getEntry(name, new PasswordProtection(pw.toCharArray()));
        return ((KeyStore.SecretKeyEntry) entry).getSecretKey();
    }

    /**
     * Guarda la llave en el archivador de llaves.
     * @param keyStoreFile Ruta y nombre del archivo del archivador de llaves.
     * @param ks Instancia del archivador de clases donde vamos a guardar la llave.
     * @param key Llave a almacenar.
     * @param pw Contraseña de la llave y del archivador.
     */
    public static void saveSecretKey(String keyStoreFile, KeyStore ks, SecretKey key, String name, String pw) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        KeyStore.SecretKeyEntry keyEntry = new SecretKeyEntry(key);
        ks.setEntry(name, keyEntry, new PasswordProtection(pw.toCharArray()));
        ks.store(new FileOutputStream(keyStoreFile), new char[0]);
    }

    /**
     * Genera un archivador de llaves y lo devuelve. En caso de que ya exista sólo lo carga y lo devuelve.
     * @param filename La ruta y nombre del archivador de llaves.
     * @return La instancia del archivador de llaves.
     */
    public static KeyStore getArchivador(String filename, int mode) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
        File f = new File(filename);
        final KeyStore ks = KeyStore.getInstance("JCEKS");
        if(f.exists()) {
            ks.load(new FileInputStream(f), new char[0]);
        } else {
            if (mode == Cipher.DECRYPT_MODE) throw new FileNotFoundException("Imposible descifrar sin tener la clave simétrica. No se pudo encontrar "
                    + System.getProperties().getProperty("user.home") + System.getProperties().getProperty("file.separator") + ".cryptofiles"
                    + System.getProperties().getProperty("file.separator") + "key.keystore");
            ks.load(null, null);
            ks.store(new FileOutputStream(f), new char[0]);
        }
        return ks;
    }
}
