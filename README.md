## If you need to change the password:  
The AES key is stored at %USERPROFILE%\\.cryptofiles
If you need to change the password, you have to delete the key.  
Be careful with not deleting the key if you have files encrypted with that key.  